Summary:             The library wraped for PKCS#11
Name:                pakchois
Version:             0.4
Release:             20 
License:             LGPLv2+
URL:                 http://www.manyfish.co.uk/pakchois/
Source0:             http://www.manyfish.co.uk/pakchois/pakchois-%{version}.tar.gz
BuildRequires:       gcc gettext

%description
The pakchois packaging library aims to provide an object-oriented C interface
that does not hide any underlying interface and avoids dependencies on any
cryptography toolkit. It is a PKCK#11 thin wapper libray.

%package devel
Summary:             Development library and C header files for the pakchois library
Requires:            pkgconfig, pakchois = %{version}-%{release}

%description devel
The pakchois PKCS#11 wrapped development library.

%prep
%autosetup -n pakchois-%{version} -p1

%build
%configure --enable-module-path=%{_libdir}/pkcs11:%{_libdir}/gnome-keyring:%{_libdir}
%make_build

%install
%make_install
%delete_la

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%doc AUTHORS NEWS README
%{_libdir}/*.so.*

%files devel
%dir %{_includedir}/%{name}
%{_includedir}/%{name}/*.h
%{_libdir}/pkgconfig/*.pc
%{_libdir}/*.so
%{_libdir}/*.a

%changelog
* Web Jun 09 2021 zhaoyao<zhaoyao32@huawei.com> - 0.4-20
- %prep no longer patched with git

* Thu Apr 30 2020 Jeffery.Gao <gaojianxing@huawei.com> - 0.4-19
- Package init
